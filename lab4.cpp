#include <GL/glew.h>
#include <GL/glu.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>

using namespace std;

struct vertex {
  float x, y;
  vertex(float x, float y) : x(x), y(y) { }
};


class pixels {
private:
  int _w, _h;

public:
  GLubyte *buffer = NULL;
  // Конструкторы
  pixels() {}

  pixels(const pixels &obj) : _w(obj._w), _h(obj._h) {
    buffer = new GLubyte[_w * _h * 4]; // на каждый пиксель нужно 4 байта потому что rgba
    memcpy(buffer, obj.buffer, _w * _h * 4);
  }

  // Основные методы
  void resize(int w, int h) {
    this->_w = w;
    this->_h = h;
    if (buffer != NULL)
      delete[] buffer;
    buffer = new GLubyte[_w * _h * 4];
    for (int i = 3; i < _w * _h * 4; i += 4)
      buffer[i] = 255;
  }

  void set_pixel(int x, int y, int R, int G, int B) {
    buffer[(_h - y) * _w * 4 + x * 4] = R;
    buffer[(_h - y) * _w * 4 + x * 4 + 1] = G;
    buffer[(_h - y) * _w * 4 + x * 4 + 2] = B;
  }

  void clear() { memset(buffer, 0, _w * _h * 4); }

  // Деструктор
  ~pixels() { delete[] buffer; }
};

std::vector<vertex> v;
pixels p;

// Основные меняющиеся переменные
int WIDTH = 500;
int HEIGHT = 500;
bool isPostFiltration = false;
bool isFilling = false;

// Горячие клавишы
const int KEY_POST_FILTRATION = GLFW_KEY_TAB;
const int KEY_CLEAR_WINDOW = GLFW_KEY_C;
const int KEY_FILL = GLFW_KEY_F;
const int KEY_CLOSE_WINDOW = GLFW_KEY_ESCAPE;


void error_callback(int code, const char *desc) { fputs(desc, stderr); }

void resize_callback(GLFWwindow *window, int w, int h) {
  p.resize(w, h);
  WIDTH = w;
  HEIGHT = h;
  glViewport(0, 0, w, h);
}

void key_callback(GLFWwindow *window, int key, int scancode, int action, int mode) {
  if (action != GLFW_RELEASE) {
    switch (key) {

      case KEY_CLEAR_WINDOW:
        p.clear();
        v.clear();
        break;

      case KEY_POST_FILTRATION:
        isPostFiltration = !isPostFiltration;
        break;

      case KEY_FILL:
        isFilling = !isFilling;
        break;

    }
  }

  if (action == GLFW_PRESS) {
    glfwSetWindowShouldClose(window, GL_TRUE);
  }
}

void mouse_callback(GLFWwindow *window, int button, int action, int mode) {
  if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
    double x, y;
    glfwGetCursorPos(window, &x, &y);
    v.push_back(vertex(x, y));
  }
}

// Брезенхем
void drawLine(int x1, int y1, int x2, int y2, int R, int G, int B) {
  const int deltaX = abs(x2 - x1);
  const int deltaY = abs(y2 - y1);
  const int signX = x1 < x2 ? 1 : -1;
  const int signY = y1 < y2 ? 1 : -1;
  int error = deltaX - deltaY;
  p.set_pixel(x2, y2, R, G, B);
  while (x1 != x2 || y1 != y2) {
    p.set_pixel(x1, y1, R, G, B);
    const int error2 = error * 2;
    if (error2 > -deltaY) {
      error -= deltaY;
      x1 += signX;
    }
    if (error2 < deltaX) {
      error += deltaX;
      y1 += signY;
    }
  }
}

// Заполнение со списком ребер и флагом
void fill() {
  bool mass[WIDTH]; // проходишься построчно и определяешь, пересекает ли ребро текущая строка или нет
  for (int y = 0; y < HEIGHT; y++) { //  y - это строки
    memset(mass, 0, sizeof(mass)); //запихнуть нули в массив
    for (int i = 0; i < v.size(); i++) { // v - vector<vertex>
      float x0, y0;
      if (i == 0) {
        x0 = v[v.size() - 1].x;
        y0 = v[v.size() - 1].y;
      } else {
        x0 = v[i - 1].x;
        y0 = v[i - 1].y;
      }
      float x1 = v[i].x;
      float y1 = v[i].y;
      if (y0 > y1) {
        std::swap(x0, x1);
        std::swap(y0, y1);
      }
      if (y + 0.5f <= y0 || y + 0.5f >= y1) {
        continue;
      }
      float dx = (x1 - x0) / (y1 - y0);
      float ix = x0 + (y - y0) * dx;
      int rix = round(ix);
      mass[rix] = !mass[rix];
    }
    bool inside = false;
    for (int x = 0; x < WIDTH; x++) {
      if (mass[x])
        inside = !inside;
      if (inside)
        p.set_pixel(x, y, 255, 255, 255);
    }
  }
}

// Взвешанная пост-фильтрация
void post_filtration() {
  pixels copy;
  copy.resize(WIDTH, HEIGHT);
  int mask[3][3] = {
    2, 4, 2,
    4, 1, 4,
    2, 4, 2
  };

  for (int i = 0; i < HEIGHT * WIDTH * 4; i += WIDTH * 4){
      for (int j = 0; j < WIDTH * 4; j += 4){
          if (j == 0 || i == 0 || j == WIDTH * 4 - 4 || i == HEIGHT * WIDTH * 4 - WIDTH * 4) {
              copy.buffer[i + j] = p.buffer[i + j];
              copy.buffer[i + j + 1] = p.buffer[i + j + 1];
              copy.buffer[i + j + 2] = p.buffer[i + j + 2];
              copy.buffer[i + j + 3] = p.buffer[i + j + 3];
          }
      }
  }

  for (int i = 0; i < HEIGHT * WIDTH * 4 - WIDTH * 4 * 2; i += WIDTH * 4) {
      for (int j = 0; j < WIDTH * 4 - 2; j += 4) {
          for (int s = 0; s < 3; s++) {
              copy.buffer[ i + WIDTH*4 + j + 4 + s] = (GLubyte) (
                      (p.buffer[i + j + s] * mask[0][0] + p.buffer[ i + WIDTH*4 + j + s] * mask[1][0] +
                       p.buffer[ i + WIDTH*4*2 + j + s] * mask[2][0] +
                       p.buffer[ i + j + 4 + s] * mask[0][1] + p.buffer[ i + WIDTH*4 + j + 4 + s] * mask[1][1] +
                       p.buffer[ i + WIDTH*4*2 + j + 4 + s] * mask[2][1] +
                       p.buffer[ i + j + 4*2 + s] * mask[0][2] + p.buffer[ i + WIDTH*4 + j + 4*2 + s] * mask[1][2] +
                       p.buffer[ i + WIDTH*4*2 + j + 4*2 + s] * mask[2][2]) / 25);
          }
      }
  }

  memcpy(p.buffer, copy.buffer, WIDTH * HEIGHT * 4);
}

// Основная render-функция
void display(GLFWwindow *window) {
  p.clear();

  // Рисование линий с помощью Брезенхема
  if (v.size() > 1) {
    drawLine((int)(v[0].x), (int)(v[0].y), (int)(v[v.size() - 1].x), (int)(v[v.size() - 1].y), 255, 255, 255);

    vertex *previus_point = &v[0];

    for (int i = 1; i < v.size(); i++) {
      drawLine((int)previus_point->x, (int)previus_point->y, (int)v[i].x, (int)v[i].y, 255, 255, 255);
      previus_point = &v[i];
    }
  }

  // Заливка
  if (isFilling) {
    fill();
  }

  // Пост-фильтрация
  if (isPostFiltration) {
    post_filtration();
  }

  // Отдача пикселей в OpenGl
  glDrawPixels(WIDTH, HEIGHT, GL_RGBA, GL_UNSIGNED_BYTE, p.buffer);
}

int main() {
  // Тексты ошибок
  const string ERROR_INIT = "Failed to init glfw";
  const string ERROR_CREATE_WINDOW = "Failed to create the window";
  const string TITLE_WINDOW = "Lab 4";

  // Инициализация GLFW
  glfwSetErrorCallback(error_callback);
  if (!glfwInit()) {
    cout <<  ERROR_INIT << endl;
    exit(1);
  }

  // Создание окна
  GLFWwindow *window = glfwCreateWindow(WIDTH, HEIGHT, TITLE_WINDOW.c_str(), NULL, NULL);
  if (!window) {
    cout << ERROR_CREATE_WINDOW << endl;
    glfwTerminate();
    exit(1);
  }

  // Установка callback функций и контекста
  glfwMakeContextCurrent(window);
  glfwSetFramebufferSizeCallback(window, resize_callback);
  glfwSetKeyCallback(window, key_callback);
  glfwSetMouseButtonCallback(window, mouse_callback);
  glClearColor(0, 0, 0, 0);

  // Первоначальная иницализация массива пикселей с текущими высотой и шириной
  resize_callback(window, WIDTH, HEIGHT);

  // Основной цикл рисовки
  while (!glfwWindowShouldClose(window)) {
    display(window);
    glfwSwapBuffers(window);
    glfwPollEvents();
  }

  // Завершение работы
  glfwDestroyWindow(window);
  glfwTerminate();
  return 0;
}
