#include <GL/glew.h>
#include <GL/glu.h>
#include <GLFW/glfw3.h>
#include <ctime>
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int WIDTH = 800, HEIGHT = 800, spin_x = 0, spin_y = 0, spin_z = 0;
float x = 0, y = 0, scale = 1;
bool fill = true, moveRight = false, move = false, coordX = false;

void error(int code, const char *desc) { fputs(desc, stderr); }

void resize(GLFWwindow *window, int w, int h) {
  glViewport(0, 0, w > h ? h : w, w > h ? h : w);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}

void key_callback(GLFWwindow *window, int key, int scancode, int action,
                  int mode) {
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    glfwSetWindowShouldClose(window, GL_TRUE);
  else if (key == GLFW_KEY_S && action != GLFW_RELEASE)
    spin_x += 5;
  else if (key == GLFW_KEY_D && action != GLFW_RELEASE)
    spin_y += 5;
  else if (key == GLFW_KEY_E && action != GLFW_RELEASE)
    spin_z += 5;

  else if (key == GLFW_KEY_W && action != GLFW_RELEASE)
    spin_x -= 5;
  else if (key == GLFW_KEY_A && action != GLFW_RELEASE)
    spin_y -= 5;
  else if (key == GLFW_KEY_Q && action != GLFW_RELEASE)
    spin_z -= 5;

  else if (key == GLFW_KEY_EQUAL && action == GLFW_PRESS)
    scale += 0.1;
  else if (key == GLFW_KEY_MINUS && action == GLFW_PRESS)
    scale -= 0.1;
  else if (key == GLFW_KEY_M && action == GLFW_PRESS)
    move = !move;
  else if (key == GLFW_KEY_TAB && action == GLFW_PRESS)
    fill = !fill;
	else if (key == GLFW_KEY_X && action == GLFW_PRESS)
		coordX = !coordX;

  else if (key == GLFW_KEY_LEFT && action != GLFW_RELEASE)
    x -= 0.1;
  else if (key == GLFW_KEY_RIGHT && action != GLFW_RELEASE)
    x += 0.1;
  else if (key == GLFW_KEY_DOWN && action != GLFW_RELEASE)
    y -= 0.1;
  else if (key == GLFW_KEY_UP && action != GLFW_RELEASE)
    y += 0.1;
}

void drawStaticCube(float x, float y, float z) {
  //
  glBegin(GL_QUADS);
  glColor3f(0, 0, 1);
  glVertex3f(x, -y, -z);
  glVertex3f(x, y, -z);
  glVertex3f(-x, y, -z);
  glVertex3f(-x, -y, -z);
  glEnd();
  //
  glBegin(GL_QUADS);
  glColor3f(0, 1, 0);
  glVertex3f(x, -y, z);
  glVertex3f(x, y, z);
  glVertex3f(-x, y, z);
  glVertex3f(-x, -y, z);
  glEnd();
  //
  glBegin(GL_QUADS);
  glColor3f(0, 1, 1);
  glVertex3f(x, -y, -z);
  glVertex3f(x, y, -z);
  glVertex3f(x, y, z);
  glVertex3f(x, -y, z);
  glEnd();
  //
  glBegin(GL_QUADS);
  glColor3f(1, 0, 0);
  glVertex3f(-x, -y, z);
  glVertex3f(-x, y, z);
  glVertex3f(-x, y, -z);
  glVertex3f(-x, -y, -z);
  glEnd();
  //
  glBegin(GL_QUADS);
  glColor3f(1, 0, 1);
  glVertex3f(x, y, z);
  glVertex3f(x, y, -z);
  glVertex3f(-x, y, -z);
  glVertex3f(-x, y, z);
  glEnd();
  //
  glBegin(GL_QUADS);
  glColor3f(1, 1, 1);
  glVertex3f(x, -y, -z);
  glVertex3f(x, -y, z);
  glVertex3f(-x, -y, z);
  glVertex3f(-x, -y, -z);
  glEnd();
}

void drawMovingCube() {
  glPushMatrix();
  glTranslatef(x, y, 0);
  glRotatef(spin_x, 1, 0, 0);
  glRotatef(spin_y, 0, 1, 0);
  glRotatef(spin_z, 0, 0, 1);
  glScalef(scale, scale, scale);
  drawStaticCube(0.2, 0.2, 0.2);
  glPopMatrix();
}

void display(GLFWwindow *window) {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  if (fill)
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  else
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  drawMovingCube();
}

void WopWopWop() {
  if (x < -0.5 * scale)
    moveRight = true;
  if (x > 0.5 * scale)
    moveRight = false;
  if (moveRight) {
    if (coordX) x += 0.02;
	}
  else {
    if (coordX) x -= 0.02;
	}
  spin_x += 3;
  spin_y += 3;
  spin_z += 3;
}

int main() {
  glfwSetErrorCallback(error);
  if (!glfwInit()) {
    std::cout << "Failed to init glfw" << std::endl;
    exit(1);
  }
  GLFWwindow *window = glfwCreateWindow(WIDTH, HEIGHT, "FirstLab", NULL, NULL);
  if (!window) {
    std::cout << "Failed to create the window" << std::endl;
    glfwTerminate();
    return 1;
  }
  glfwMakeContextCurrent(window);
  glfwSetFramebufferSizeCallback(window, resize);
  glfwSetKeyCallback(window, key_callback);
  glEnable(GL_DEPTH_TEST);
  glClearColor(0, 0, 0, 0);
  resize(window, WIDTH, HEIGHT);
  while (!glfwWindowShouldClose(window)) {
    if (move)
      WopWopWop();
    display(window);
    glfwSwapBuffers(window);
    glfwPollEvents();
  }
  glfwDestroyWindow(window);
  glfwTerminate();
  return 0;
}
