#include <GL/glew.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <vector>

using namespace std;

int WIDTH = 600, HEIGHT = 600, spin_x = 0, spin_y = 0, spin_z = 0, step = 50;
float x = 0, y = 0, scale = 1;
bool Fill = true, move = false, moveRight = false, coordX = false,
     krishecka = false;

float pi = 3.14159265, phi = pi / 4, tetta = pi / 5.14;

float m[16] = {cos(phi),
               sin(phi) * sin(tetta),
               sin(phi) * cos(tetta),
               0,
               0,
               cos(tetta),
               -sin(tetta),
               0,
               sin(phi),
               -cos(phi) * sin(tetta),
               -cos(phi) * cos(tetta),
               0,
               0,
               0,
               0,
               1};

struct vertex3f {
  float x, y, z;
};

struct v2 {
  vertex3f first, second;
};

v2 **v, **k;
bool init = false;

void CalculateVertexs(double radius, double h) {
  int massI = 0, massJ;
  if (init) {
    free(v);
    free(k);
  } else
    init = !init;
  v = (v2 **)malloc(sizeof(v2 *) * (step + 1));
  k = (v2 **)malloc(sizeof(v2 *) * (step + 1));
  for (int i = 0; i < step + 1; i++)
    k[i] = new v2[step + 1];
  for (int i = 0; i < step + 1; i++)
    v[i] = new v2[step + 1];
  for (double i = 0; i <= 2 * pi + 0.0001; i += 2 * pi / step, massI++) {
    massJ = 0;
    for (double j = 0; j <= h + 0.0001; j += h / step, massJ++) {
      v[massI][massJ].first.x = radius * cos(i) + j;
      v[massI][massJ].first.y = j;
      v[massI][massJ].first.z = radius * sin(i);
      v[massI][massJ].second.x = radius * cos(i + 2 * pi / step) + j;
      v[massI][massJ].second.y = j;
      v[massI][massJ].second.z = radius * sin(i + 2 * pi / step);
    }
    massJ = 0;
    for (double r1 = radius; r1 >= 0; r1 -= radius / step, massJ++) {
      k[massI][massJ].first.x = r1 * cos(i);
      k[massI][massJ].first.y = 0;
      k[massI][massJ].first.z = r1 * sin(i);
      k[massI][massJ].second.x = r1 * cos(i + 2 * pi / step);
      k[massI][massJ].second.y = 0;
      k[massI][massJ].second.z = r1 * sin(i + 2 * pi / step);
    }
  }
}

void WopWopWop() {
  if (x < -0.5 * scale)
    moveRight = true;
  if (x > 0.5 * scale)
    moveRight = false;
  if (moveRight) {
    if (coordX)
      x += 0.02;
  } else {
    if (coordX)
      x -= 0.02;
  }
  spin_x += 3;
  spin_y += 3;
  spin_z += 3;
}

void error(int code, const char *desc) { fputs(desc, stderr); }

void resize(GLFWwindow *window, int w, int h) {
  glViewport(0, 0, w > h ? w : h, w > h ? w : h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glMultMatrixf(m);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}

void key_callback(GLFWwindow *window, int key, int scancode, int action,
                  int mode) {
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    glfwSetWindowShouldClose(window, GL_TRUE);
  else if (key == GLFW_KEY_S && action != GLFW_RELEASE)
    spin_x += 5;
  else if (key == GLFW_KEY_W && action != GLFW_RELEASE)
    spin_x -= 5;
  else if (key == GLFW_KEY_D && action != GLFW_RELEASE)
    spin_y += 5;
  else if (key == GLFW_KEY_A && action != GLFW_RELEASE)
    spin_y -= 5;
  else if (key == GLFW_KEY_E && action != GLFW_RELEASE)
    spin_z += 5;
  else if (key == GLFW_KEY_Q && action != GLFW_RELEASE)
    spin_z -= 5;

  else if (key == GLFW_KEY_EQUAL && action != GLFW_RELEASE)
    scale += 0.1;
  else if (key == GLFW_KEY_MINUS && action != GLFW_RELEASE)
    scale -= 0.1;

  else if (key == GLFW_KEY_M && action == GLFW_PRESS)
    move = !move;
  else if (key == GLFW_KEY_TAB && action == GLFW_PRESS)
    Fill = !Fill;
  else if (key == GLFW_KEY_X && action == GLFW_PRESS)
    coordX = !coordX;
  else if (key == GLFW_KEY_R && action == GLFW_PRESS)
    spin_x = spin_y = spin_z = 0;
  else if (key == GLFW_KEY_K && action == GLFW_PRESS)
    krishecka = !krishecka;

  else if (key == GLFW_KEY_LEFT && action != GLFW_RELEASE)
    x -= 0.1;
  else if (key == GLFW_KEY_RIGHT && action != GLFW_RELEASE)
    x += 0.1;
  else if (key == GLFW_KEY_DOWN && action != GLFW_RELEASE)
    y -= 0.1;
  else if (key == GLFW_KEY_UP && action != GLFW_RELEASE)
    y += 0.1;

  else if (key == GLFW_KEY_LEFT_BRACKET && action != GLFW_RELEASE) {
    if (step > 3) {
      step--;
      CalculateVertexs(0.3, 0.5);
      printf("\r%3d", step);
      fflush(stdout);
    }
  } else if (key == GLFW_KEY_RIGHT_BRACKET && action != GLFW_RELEASE) {
    step++;
    CalculateVertexs(0.3, 0.5);
    printf("\r%3d", step);
    fflush(stdout);
  }
}

void drawCube(float x) {
  glBegin(GL_QUADS);
  glColor3f(0, 0, 1);
  glVertex3f(x, -x, -x);
  glVertex3f(x, x, -x);
  glVertex3f(-x, x, -x);
  glVertex3f(-x, -x, -x);
  glEnd();
  glBegin(GL_QUADS);
  glColor3f(0, 1, 0);
  glVertex3f(x, -x, x);
  glVertex3f(x, x, x);
  glVertex3f(-x, x, x);
  glVertex3f(-x, -x, x);
  glEnd();
  glBegin(GL_QUADS);
  glColor3f(0, 1, 1);
  glVertex3f(x, -x, -x);
  glVertex3f(x, x, -x);
  glVertex3f(x, x, x);
  glVertex3f(x, -x, x);
  glEnd();
  glBegin(GL_QUADS);
  glColor3f(1, 0, 0);
  glVertex3f(-x, -x, x);
  glVertex3f(-x, x, x);
  glVertex3f(-x, x, -x);
  glVertex3f(-x, -x, -x);
  glEnd();
  glBegin(GL_QUADS);
  glColor3f(1, 0, 1);
  glVertex3f(x, x, x);
  glVertex3f(x, x, -x);
  glVertex3f(-x, x, -x);
  glVertex3f(-x, x, x);
  glEnd();
  glBegin(GL_QUADS);
  glColor3f(1, 1, 1);
  glVertex3f(x, -x, -x);
  glVertex3f(x, -x, x);
  glVertex3f(-x, -x, x);
  glVertex3f(-x, -x, -x);
  glEnd();
}

void drawKrishecka(double radius, double h) {
  int massI = 0, massJ;
  for (double i = 0; i <= 2 * pi + 0.0001; i += 2 * pi / step, massI++) {
    glBegin(GL_QUAD_STRIP);
    massJ = 0;
    for (double r1 = radius; r1 >= 0; r1 -= radius / step, massJ++) {
      // cout << massI << " " << massJ << " " << step << endl;
      glVertex3f(k[massI][massJ].first.x, k[massI][massJ].first.y,
                 k[massI][massJ].first.z);
      glVertex3f(k[massI][massJ].second.x, k[massI][massJ].second.y,
                 k[massI][massJ].second.z);
    }
    glVertex3f(0, 0, 0);
    glVertex3f(0, 0, 0);
    glEnd();
    glBegin(GL_QUAD_STRIP);
    massJ = 0;
    for (double r1 = radius; r1 >= 0; r1 -= radius / step, massJ++) {
      // cout << massI << " " << massJ << " " << step << endl;
      glVertex3f(k[massI][massJ].first.x + h, k[massI][massJ].first.y + h,
                 k[massI][massJ].first.z);
      glVertex3f(k[massI][massJ].second.x + h, k[massI][massJ].second.y + h,
                 k[massI][massJ].second.z);
    }
    glVertex3f(h, h, 0);
    glVertex3f(h, h, 0);
    glEnd();
  }
}

void drawCilindre(double radius, double h) {
  glTranslatef(-h / 2, -h / 2, 0);
  if (krishecka) {
    drawKrishecka(radius, 0);
    drawKrishecka(radius, h);
  }
  int massI = 0, massJ;
  for (double i = 0; massI < step; i += 2 * pi / step, massI++) {
    glColor3f(1, 1, 0);
    glBegin(GL_QUAD_STRIP);
    massJ = 0;
    for (double j = 0; massJ < step; j += h / step, massJ++) {
      glVertex3f(v[massI][massJ].first.x, v[massI][massJ].first.y,
                 v[massI][massJ].first.z);
      glVertex3f(v[massI][massJ].second.x, v[massI][massJ].second.y,
                 v[massI][massJ].second.z);
    }
    glVertex3f(radius * cos(i) + h, h, radius * sin(i));
    glVertex3f(radius * cos(i + 2 * pi / step) + h, h,
               radius * sin(i + 2 * pi / step));
    glEnd();
  }
}

void Move() {
  glPushMatrix();
  glTranslatef(x, y, 0);
  glRotatef(spin_x, 1, 0, 0);
  glRotatef(spin_y, 0, 1, 0);
  glRotatef(spin_z, 0, 0, 1);
  glScalef(scale, scale, scale);
}

void display(GLFWwindow *window) {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  if (Fill)
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  else
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  Move();
  drawCilindre(0.3, 0.5);
  glPopMatrix();
  glPushMatrix();
  glTranslatef(-0.95, -0.5, 0);
  drawCube(0.1);
  glPopMatrix();
}

int main() {
  glfwSetErrorCallback(error);
  if (!glfwInit()) {
    cout << "Failed to init glfw" << endl;
    exit(1);
  }
  GLFWwindow *window = glfwCreateWindow(WIDTH, HEIGHT, "Lab", NULL, NULL);
  if (!window) {
    cout << "Failed to create the window" << endl;
    glfwTerminate();
    exit(1);
  }
  glfwMakeContextCurrent(window);
  glfwSetFramebufferSizeCallback(window, resize);
  glfwSetKeyCallback(window, key_callback);
  glEnable(GL_DEPTH_TEST);
  glClearColor(0, 0, 0, 0);
  resize(window, WIDTH, HEIGHT);
  CalculateVertexs(0.3, 0.5);
  while (!glfwWindowShouldClose(window)) {
    if (move)
      WopWopWop();
    display(window);
    glfwSwapBuffers(window);
    glfwPollEvents();
  }
  glfwDestroyWindow(window);
  glfwTerminate();
  free(v);
  free(k);
  return 0;
}
