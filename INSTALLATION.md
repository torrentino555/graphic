# Requirements #
* OS Ubuntu
* Internet

# Installation #

For install any library writing this code into your console:

```
sudo apt-get install ${ library_name }
```

# Libraries #
* freeglut3
* freeglut3-dev
* libglu1-mesa
* libglu1-mesa-dev
* libglfw3
* libglfw3-dev
* libglew-dev
